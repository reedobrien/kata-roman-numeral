package main

import "errors"

func convert(i int) (string, error) {
	val, ok := numerals[i]
  if !ok {
    return "", errors.New("Invalid number")
  }
  return val, nil
}

var numerals = map[int]string{
	1:    "I",
	5:    "V",
	10:   "X",
	50:   "L",
	100:  "C",
	500:  "D",
	1000: "M",
}
