package main

import "testing"

func TestBaseRomanNumerals(t *testing.T) {
	table := []struct {
		input  int
		want   string
		errMsg string
		nilErr bool
	}{
    {0, "", "Invalid number", false},
		{1, "I", "", true},
		{10, "X", "", true},
		{100, "C", "", true},
		{1000, "M", "", true},
		// {1900, "MCM"},
		// {1950, "MCML"},
		// {1990, "MCMXC"},
		// {2008, "MMVIII"},
		// {2, "II"},
		// {24, "XXIV"},
		// {39, "XXXIX"},
		// {3, "III"},
		// {4, "IV"},
		{5, "V", "", true},
		{50, "L", "", true},
		{500, "D", "", true},
	}
	for i, test := range table {
		got, err := convert(test.input)
		if !test.nilErr {
      if err == nil {
        t.Fatalf("test %d failed: got nil error, expected %s", i, test.errMsg)
      }
			if err.Error() != test.errMsg {
				t.Errorf("test %d failed: got errMsg %s, wanted %s", i, err.Error(), test.errMsg)
			}
			continue
		}
		if got != test.want {
			t.Errorf("test %d failed: got %s, want %s", i, got, test.want)
		}
		if err != nil {
			t.Errorf("test %d failed: got error %s, wanted nil error", i, err.Error())
		}
	}
}
